<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/26
 */

namespace models;


use think\Model;

class Setting extends Model
{

    protected $table = 'cms_setting';


    static public function getString(string $key): string
    {
        $model = self::where('keys', $key)
            ->field('value')->findOrEmpty();
        if ($model->isEmpty()) {
            return '';
        }
        return trim($model->value);
    }

    static public function getInteger(string $key): int
    {
        $value = self::getString($key);
        return intval($value);
    }

    static public function getArray(string $key): array
    {
        $value = self::getString($key);
        return json_decode($value, true);
    }

    static public function getObject(string $key)
    {
        $value = self::getString($key);
        return json_decode($value);
    }

}
