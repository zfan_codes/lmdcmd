<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/28
 */

namespace models;


use think\Model;

class Page extends Model
{

    protected $table = 'cms_page';

    static public function getInstance(int $id): self
    {
        return self::whereRaw('sort_id=?', [$id])->findOrEmpty();
    }

    public function getLeft()
    {
        return self::where('id', $this->left_id)->findOrEmpty();
    }

}
