<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/27
 */

namespace models;


use think\Model;

class Slides extends Model
{

    protected $table = 'cms_slides';

    static public function getAll(int $num)
    {
        return self::whereRaw('state=1')
            ->limit($num)
            ->order('sort asc,id desc')->select();
    }

}
