<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/27
 */

namespace models;


use think\Collection;
use think\Model;

class Links extends Model
{

    protected $table = 'cms_links';


    static public function findAll(): Collection
    {
        return self::whereRaw('state=1')
            ->order('sort asc,id desc')->select();
    }

}
