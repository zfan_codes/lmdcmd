<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/28
 */

namespace models;


use think\Model;

class Article extends Model
{

    protected $table = 'cms_article';

    static public function getInstance(int $id): self
    {
        return self::whereRaw('id=?', [$id])->findOrEmpty();
    }

}
