<?php
declare(strict_types=1);

/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/26
 * 绑定类
 */

use app\index\service\SettingPageService;

return [
    'page' => SettingPageService::class
];
