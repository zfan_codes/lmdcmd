<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/27
 */

namespace app\index\service;


use models\Slides;

class IndexService
{

    public function getSlides()
    {
        return Slides::getAll(4);
    }

}
