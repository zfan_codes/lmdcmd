<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/28
 */

namespace app\index\service;


use models\Article;
use models\Sorts;

class ArticleService
{

    private $sort;

    /**
     * 验证是否是文章列表
     * @param int $id
     * @return bool
     */
    public function checkArticle(int $id): bool
    {
        $sort = Sorts::getInstance($id);
        if ($sort->isEmpty()) {
            return false;
        }
        $this->sort = $sort;
        if (in_array($sort->type, [4, 5])) {
            return true;
        }
        return false;
    }

}
