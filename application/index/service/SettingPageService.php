<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/26
 */

namespace app\index\service;


use models\Article;
use models\Links;
use models\Setting;
use models\Sorts;
use think\Collection;
use think\facade\Cache;

class SettingPageService
{

    /**
     * 获取配置
     */
    public function get(string $key): string
    {
        $ckey = 'setting-' . md5($key);
        if ($res = Cache::has($ckey)) {
            return Cache::get($ckey);
        }
        $res = Setting::getString($key);
        Cache::tag('setting')->set($ckey, $res, 3600 * 24 * 30 * 12);
        return $res;
    }

    /**
     * 获取文章列表
     */
    public function getArticle(string $sql, int $limit = 10, string $field = "*"): Collection
    {
        return Article::where($sql)->order('id desc')
            ->fieldRaw($field)->limit($limit)->select();
    }

    /**
     * 获取Logo文件路径
     * @return string
     */
    public function getLogo(): string
    {
        return $this->get('index.page.icon');
    }

    /**
     * 获取标语图片
     * @return string
     */
    public function getSlogan(): string
    {
        return $this->get('index.page.ad');
    }

    /**
     * 获取所有菜单
     * @return array
     */
    public function getMenus(): array
    {
        $key = 'setting-menus';
        if ($res = Cache::get($key)) {
            return $res;
        }
        $res = Sorts::getMenus();
        Cache::tag('setting')->set($key, $res, 3600 * 24 * 30 * 12);
        return $res;
    }

    /**
     * 获取联系电话
     * @return string
     */
    public function getPhone(): string
    {
        return $this->get('index.page.phone');
    }

    /**
     * 获取地址
     * @return string
     */
    public function getAddress(): string
    {
        return $this->get('index.page.address');
    }

    /**
     * 获取网址
     * @return string
     */
    public function getUrl(): string
    {
        return $this->get('index.page.url');
    }


    /**
     * 获取公司名称
     * @return string
     */
    public function getCompany(): string
    {
        return $this->get('globl.page.title');
    }

    /**
     * 获取友情连接
     * @return array
     */
    public function getLinks()
    {
        $key = 'setting-links';
        if ($res = Cache::get($key)) {
            return $res;
        }
        $res = Links::findAll();
        Cache::tag('setting')->set($key, $res, 3600 * 24 * 30 * 12);
        return $res;
    }

    /**
     * 网站名称
     * @return string
     */
    public function getTitle(): string
    {
        return $this->get('globl.page.title');
    }

    /**
     * 获取网站关键字
     * @return string
     */
    public function getKeywords(): string
    {
        return $this->get('globl.page.keywords');
    }

    /**
     * 网站描述
     * @return string
     */
    public function getDescription(): string
    {
        return $this->get('globl.page.description');
    }
}
