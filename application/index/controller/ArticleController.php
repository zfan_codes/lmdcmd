<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/28
 */

namespace app\index\controller;


use app\index\paginator\NewPage;
use app\index\service\ArticleService;
use models\Article;
use models\Sorts;
use think\Controller;
use think\Request;

class ArticleController extends Controller
{

    public function ListAction(Request $request)
    {
        $id = $request->get('id', 0);
        $sort = Sorts::getInstance(intval($id));
        if ($id == 0 || $sort->isEmpty() || !in_array($sort->type, [4, 5])) {
            abort(404);;
        }

        $pageInfo = Article::where('sort_id', $id)
            ->whereRaw('is_show=1 and state=1')
            ->order('sort asc,id desc')
            ->paginate(10, true, [
                'query' => $request->get(),
                'type' => NewPage::class
            ]);
        return $this->fetch('list', [
            'pageInfo' => $pageInfo,
            'sortInfo' => $sort
        ]);
    }


    public function detailAction(Request $request)
    {
        $id = $request->get('id', 0);
        $article = Article::getInstance(intval($id));
        if ($article->isEmpty()) {
            abort(404);
        }

        return $this->fetch('detail', ['info' => $article]);
    }

}
