<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/28
 */

namespace app\index\controller;


use models\Page;
use think\Controller;
use think\Request;

class PageController extends Controller
{

    public function showAction(Request $request)
    {
        $id = $request->get('id');
        $info = Page::getInstance(intval($id));
        if ($info->isEmpty()) {
           return abort(404);
        }

        $left = $info->getLeft();
        if (!$left->isEmpty()){
            return $this->fetch('show', compact('info','left'));
        }
        return $this->fetch('detail', compact('info'));
    }
}
