<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/28
 */

namespace app\index\controller;


use app\index\paginator\NewPage;
use models\Album;
use models\Sorts;
use think\Controller;
use think\Request;

class AlbumController extends Controller
{

    public function listAction(Request $request)
    {
        $id = $request->get('id', 0);
        $sortInfo = Sorts::getInstance(intval($id));
        if ($sortInfo->isEmpty()) {
            abort(404);
        }

        $pageInfo = Album::where('sort_id', $sortInfo->id)
            ->order('sort asc,id desc')->paginate(20, true, [
                'query' => $request->get(),
                'type' => NewPage::class
            ]);
        return $this->fetch('list', compact('pageInfo','sortInfo'));
    }

}
