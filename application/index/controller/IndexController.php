<?php
declare(strict_types=1);
/**
 * Create 范钟<coder.zf@outlook.com> 2019/7/26
 */

namespace app\index\controller;


use app\index\service\IndexService;
use think\Controller;

class IndexController extends Controller
{

    public function IndexAction(IndexService $service)
    {
        $slides = $service->getSlides();
        return $this->fetch('index', compact('slides'));
    }

}
