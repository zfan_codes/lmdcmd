/*
SQLyog Ultimate v13.1.1 (64 bit)
MySQL - 5.7.26 : Database - lmdcms
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`lmdcms` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `lmdcms`;

/*Table structure for table `cms_album` */

DROP TABLE IF EXISTS `cms_album`;

CREATE TABLE `cms_album` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '相册地址',
  `sort_id` int(11) NOT NULL COMMENT '分类ID',
  `sort` int(11) NOT NULL DEFAULT '50' COMMENT '排序',
  PRIMARY KEY (`id`),
  KEY `sort_id` (`sort_id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_album` */

insert  into `cms_album`(`id`,`title`,`url`,`sort_id`,`sort`) values 
(1,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(2,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(3,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(4,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(5,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(6,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(7,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(8,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(9,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(10,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(11,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50),
(12,'1111','https://s2.ax1x.com/2019/07/28/elHyi6.jpg',4,50);

/*Table structure for table `cms_article` */

DROP TABLE IF EXISTS `cms_article`;

CREATE TABLE `cms_article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `atype` tinyint(4) NOT NULL DEFAULT '1' COMMENT '类型 1 文章 2 图片',
  `sort_id` int(11) NOT NULL COMMENT '分类ID',
  `title` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '文章标题',
  `content` text COLLATE utf8mb4_unicode_ci COMMENT '内容',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否显示',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1 正常',
  `sort` int(11) NOT NULL DEFAULT '50' COMMENT '排序 小的在前',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_article` */

insert  into `cms_article`(`id`,`atype`,`sort_id`,`title`,`content`,`is_show`,`state`,`sort`,`created_at`,`updated_at`) values 
(1,1,2,'影楼和摄影工作室的6大区别','<p class=\"introduction\">\r\n                导读：根据需求选择最重要            </p>\r\n            <p><span style=\"font-size: 16px;\">在拍摄婚纱照前，新人往往迷茫如何选择一家适合自己的摄影工作室或影楼，甚至不了解如何去区分它们。这里总结了他俩之间的6大区别，以供参考。</span></p><p><img src=\"http://img0.easywed.cn/attachs/2017/04/28/87435c1ef9f424eb3bc6e49149b16056.jpg@1280w.jpg\" title=\"/87435c1ef9f424eb3bc6e49149b16056\" alt=\"/女神预告3.jpg\"></p><p><br></p><p><strong><span style=\"font-size: 16px;\">1. 规模上的不同</span></strong></p><p><span style=\"font-size: 16px;\">影楼：地理位置较优越，场地较大。</span></p><p><span style=\"font-size: 16px;\">摄影工作室：地理位置一般不会处于商业繁华地段，场地相对较小，但同时投资少，价格性价比也更高。</span></p><p><span style=\"font-size: 16px;\"><br></span></p><p><strong><span style=\"font-size: 16px;\">2. 服装上的不同</span></strong></p><p><span style=\"font-size: 16px;\">影楼：服装较多，按套系将婚纱分为不同区域和等级，在很多普通价位套系中，所列举的精品区和贵宾区，新人们基本上选不到什么合适的衣服，漂亮时尚的婚纱，影楼都将它们放在VIP区，所以，想穿那些漂亮婚纱拍照的MM，不得不掏腰包。</span></p><p><span style=\"font-size: 16px;\">摄影工作室：婚纱基本都是免费随意挑选，婚纱的款式、样式与影楼相比，也更新颖时尚，不是太大众化。</span></p><p><span style=\"font-size: 16px;\"><img src=\"http://img0.easywed.cn/attachs/2017/04/28/d8baefe4878a88229ec453b5b8031ccf.jpg@1280w.jpg\" title=\"/d8baefe4878a88229ec453b5b8031ccf\" alt=\"/月亮公主.jpg\"></span></p><p><span style=\"font-size: 16px;\"><br></span></p><p><strong><span style=\"font-size: 16px;\">3. 妆束上的不同</span></strong></p><p><span style=\"font-size: 16px;\">影楼：化妆造型上走流水式操作，存在着一个现象，很多影楼会向顾客推荐安瓶（浓缩精华），让消费者掏腰包。很多时候，消费者会很无奈，不用他们的安瓶，又怕他们不好好给你化妆。但价格确实不便宜，100块钱一瓶，用个四、五支的也不少钱，但市场购买稍好些的也不是很贵。</span></p><p><span style=\"font-size: 16px;\">摄影工作室：不用考虑他们会利用安瓶赚你的钱，反而会建议你自己去买，市场上最便宜的安瓶10块钱也够用。化妆技术和影楼相比，会更专业些，因为工作室的命脉就是技术，顾客相对少，所以在服务上肯定比影楼好，不会存在大批发现象。</span></p><p><span style=\"font-size: 16px;\"><br></span></p><p><strong><span style=\"font-size: 16px;\">4. 拍摄上的不同</span></strong></p><p><span style=\"font-size: 16px;\">影楼：有很多的内景场地，拍摄时多采用流水模式，不同套系有不同摄影师，一个摄影师只对一种或者几种套系进行拍摄，拍摄效果比较单一，如果在旺季，会有一个摄影师给三四对新人拍照的雷同现象。</span></p><p><span style=\"font-size: 16px;\">摄影工作室：多为外景拍摄，更注重创意个性的拍摄手法，拍摄时采用一对一的服务，一天只拍摄一对新人。摄影师会根据新人特点进行量身定做，拍摄前期进行双方沟通，拍出的效果绝对比影楼更精湛。</span></p><p><span style=\"font-size: 16px;\"><img src=\"http://img0.easywed.cn/attachs/2017/04/28/3dcaaaab0804417163065143f33fc018.jpg@1280w.jpg\" title=\"/3dcaaaab0804417163065143f33fc018\" alt=\"/美新娘2.jpg\"></span></p><p><span style=\"font-size: 16px;\"><br></span></p><p><strong><span style=\"font-size: 16px;\">5. 消费上的不同</span></strong></p><p><span style=\"font-size: 16px;\">影楼：选择价位套系后，对入册张数进行了限制，后期会促使你二次消费。如若购买剩下的照片，一般一张在60元左右。</span></p><p><span style=\"font-size: 16px;\">摄影工作室：是在前期消费，因为套系入册本来就很多，还全部赠送底片，这就避免了后期二次消费的问题。</span></p><p><span style=\"font-size: 16px;\"><br></span></p><p><strong><span style=\"font-size: 16px;\">6. 后期制作的不同</span></strong></p><p><span style=\"font-size: 16px;\">影楼：大部分是一键模式化修图再微调。</span></p><p><span style=\"font-size: 16px;\">摄影工作室：更偏重气氛、背景、灯光等方面的制作，修完片以后感觉自己好像明星一样呢。</span></p>        ',1,1,50,'2019-07-28 17:20:54','2019-07-28 17:20:54'),
(2,1,2,'7个小准备助你拍出明星级婚礼好照片！','\r\n            <h1 class=\"content-title\"> 7个小准备助你拍出明星级婚礼好照片！</h1>\r\n            <p class=\"title-subhead\">\r\n                <span>2017.03.31</span>\r\n                <span>17:01</span>\r\n                <span>美薇亭婚礼顾问</span>\r\n            </p>\r\n            <p class=\"introduction\">\r\n                导读：简单的小准备，有效提升婚礼照片品质~            </p>\r\n            <p class=\"p1\">\r\n	<img src=\"http://img0.easywed.cn/attachs/2017/03/31/3c643664e666fd5a068b890b66d4e636.jpg\" alt=\"\"> \r\n</p>\r\n<p class=\"p1\" style=\"text-align:center;\">\r\n	<br>\r\n</p>\r\n<p class=\"p1\" style=\"text-align:left;\">\r\n	<span style=\"color:#333333;font-size:16px;line-height:24px;\">今天我们聊聊婚礼之前，可以提前做哪些简单的准备，从而有效的提升婚礼照片品质。</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\"><span style=\"color:#999999;\">1、</span></span><span style=\"font-size:16px;color:#999999;line-height:1.5;\"> </span><span style=\"font-size:16px;color:#999999;line-height:1.5;\"> 一套精心准备的新娘内衣|</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\">你不可错过的写真时刻！</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">一套合身的内衣是婚纱的灵魂，</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">可以扬长避短改善身形完美衬托婚纱的曲线</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">。而除了穿在内部需要贴合，精心准备的内衣更要美丽，都说结婚是少女时代的结束，那么婚礼中的写真就是我们给自己最好的礼物。</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">等等，你说太香艳性感了hold不住？美</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">姐认为美不是千篇一律的，鼓励各位留下真实的样子，但如果你是唯美清新的淑女，那睡袍会是你不错的选择。</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<img src=\"http://img0.easywed.cn/attachs/2017/03/31/fc1a9856b6032edc80209851ce920256.jpg\" alt=\"\"> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#999999;line-height:1.5;\">2、</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\"> 一个漂亮的全身穿衣镜|</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\">并不只是为了化妆所选！</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"color:#333333;font-size:16px;line-height:1.5;\">如果我们没有豪华的欧式房间、梦幻的公主床。至少你可以有一面漂亮的镜子,除了化妆，它远比你想象的更有用。镜中照是全球新娘不可错过的婚礼照片之一，甚至很多国际婚礼摄影大赛准备环节的获奖照片都出自于镜子的灵感创意。</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<img src=\"http://img0.easywed.cn/attachs/2017/03/31/2e8d5f06f4e1d850ee19ed79fed05796.jpg\" alt=\"\"> \r\n</p>\r\n<p class=\"p2\">\r\n	<span style=\"font-size:16px;color:#999999;line-height:1.5;\">3、</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\"> 一个精致的衣挂|</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\">为婚纱找一个安放之处！</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">白色的蕾丝、璀璨的镀金、闪耀的镶钻，</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">如果我们千辛万苦精挑细选了一件婚纱</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">，那与之相配的衣挂，到底应该多精致？</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"color:#333333;font-size:16px;line-height:1.5;\">酒店标配洗衣店赠送，它们并没错，但我想你已不想要。</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<img src=\"http://img0.easywed.cn/attachs/2017/03/31/ea17a7ae7228e18256c78def1260c5c3.jpg\" alt=\"\"> \r\n</p>\r\n<p class=\"p2\">\r\n	<span style=\"font-size:16px;color:#999999;line-height:1.5;\">4、</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\"> 伴娘&amp;伴郎|</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\">你的婚礼需要加点色彩！</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"color:#333333;font-size:16px;line-height:1.5;\">如果你有一群好闺蜜，他有一群好基友，没有什么比他们更能惊艳你的婚礼了。而你要做的仅仅是给他们增加一点色彩！</span> \r\n</p>\r\n<p class=\"p2\">\r\n	<span style=\"color:#333333;font-size:16px;line-height:1.5;\"><span style=\"color:#333333;font-size:16px;line-height:24px;\"><strong>◆</strong></span><strong></strong><strong>tips</strong></span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">伴郎的袜子、袖扣、领带，伴娘的睡袍、高跟鞋、指甲，都是不错的细节颜色搭配选择。</span> \r\n</p>\r\n<p class=\"p2\">\r\n	<img src=\"http://img0.easywed.cn/attachs/2017/03/31/0e391a3a3c586a3e44a9cd3a39d2d976.jpg\" alt=\"\"> \r\n</p>\r\n<p class=\"p1\">\r\n	<img src=\"http://img0.easywed.cn/attachs/2017/03/31/6c41fabfa2acdab0436b9ce1300fc2d3.jpg\" alt=\"\"> \r\n</p>\r\n<p class=\"p2\">\r\n	<span style=\"font-size:16px;color:#999999;line-height:1.5;\">5</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\"> 、百元小道具|</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\">给婚礼带来大大惊喜！</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">一百元以内的花费</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">，带来的惊喜。这样的交换你拒绝么？</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\"><span style=\"font-size:16px;color:#333333;line-height:1.5;\">气球、纸伞、搞怪道具，俏皮又趣味，只是不知道，不是想不到，现在你想到的一定比美姐多。</span></span> \r\n</p>\r\n<p class=\"p1\">\r\n	<img src=\"http://img0.easywed.cn/attachs/2017/03/31/35ba88700dcfa723e128e5ba2b04c3d2.jpg\" alt=\"\"> \r\n</p>\r\n<p class=\"p3\" style=\"text-align:center;\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\"><span style=\"font-size:14px;\"></span><span style=\"font-size:14px;color:#999999;\">RONG FENG KE -WPPI 获奖作品</span></span> \r\n</p>\r\n<p class=\"p3\" style=\"text-align:center;\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\"><span style=\"font-size:14px;color:#999999;\"> </span><span style=\"font-size:14px;color:#999999;\"><img src=\"http://img0.easywed.cn/attachs/2017/03/31/a32df383b204234ac0d3da91b31f41b1.jpg\" alt=\"\"></span></span><span style=\"font-size:14px;color:#999999;line-height:1.5;\">DM Chung 香港- WPJA获奖作品</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#999999;line-height:1.5;\">6、</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\"> 一小时以内|</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\">给婚礼留些拍照时间！</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">婚礼那么忙，没有1小时？我是说如果，如果你可以，请留出1小时，拿去拍照或者合影，</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">绝对不会让你失望！</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\"><img src=\"http://img0.easywed.cn/attachs/2017/03/31/0bedb6c24a88453568dada8f88bd9fd9.jpg\" alt=\"\"><br>\r\n</span> \r\n</p>\r\n<p class=\"p2\">\r\n	<span style=\"font-size:16px;color:#999999;line-height:1.5;\">7、</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\"> dress code|</span><span style=\"font-size:16px;color:#999999;line-height:1.5;\">婚礼该有的礼仪！</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">谈起婚礼的着装要求，美姐其实是</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">谨慎的</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">，我们经常问为什么外国的婚礼总是看起来更洋气？真不是崇洋媚外，</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">外国婚礼大合影打开画风</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">是这样的</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<img src=\"http://img0.easywed.cn/attachs/2017/03/31/79597f7603b418d8afe852bd9b384ad5.jpg\" alt=\"\"> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">而中国的画风是...</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">此处应该有照片</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">，但为了不冒犯任何人，美姐没有放，相信大家不会陌生，请自动脑补。</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">我知道这个要求，中国近代比较没有着装要求传统，婚礼又比较大型很难操作，需要每一位来宾的配合，</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">但罗马不是一天建成，从给来宾一个dress code开始。</span> \r\n</p>\r\n<p class=\"p4\">\r\n	<img src=\"http://img0.easywed.cn/attachs/2017/03/31/24c1fe7bd6468bf8377457fe0e68c24c.jpg\" alt=\"\"> \r\n</p>\r\n<p class=\"p4\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\"><span style=\"color:#333333;font-size:16px;line-height:24px;\"><strong>◆</strong></span><strong></strong><strong>tips</strong></span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">着装要求适宜在发送纸质或电子请柬以及短信等各种邀请函的时候，随信附上，措词礼貌，西方有成套的着装规范，中国让我们从正装开始。</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\">最后,</span><span style=\"font-size:16px;color:#333333;line-height:1.5;\">愿你也有经典难忘的婚礼照片。</span> \r\n</p>\r\n<p class=\"p1\">\r\n	<span style=\"font-size:16px;color:#333333;line-height:1.5;\"><img src=\"http://img0.easywed.cn/attachs/2017/03/31/d6b2376e70b302c3d2ebdf29dd526690.jpg\" alt=\"\"><br>\r\n</span> \r\n</p>\r\n<p class=\"p1\" style=\"text-align:center;\">\r\n	<span style=\"color:#999999;font-size:14px;\">Joe Buissink 美国十大明星婚礼摄影师</span> \r\n</p>        ',1,1,50,'2019-07-28 17:24:24','2019-07-28 17:24:24'),
(3,1,2,'一生难忘的主角时刻！婚礼摄像怎么选？','导读：好的婚礼摄像，可以完美的重现婚礼当天的气氛，留下最难忘的回忆！ ',1,1,50,'2019-07-28 17:25:10','2019-07-28 17:25:10'),
(4,1,2,'婚礼照片告别遗憾，50个婚礼瞬间拍摄清单','导读：掌握这几点秘诀，让你的婚礼照片难忘终生 ',1,1,50,'2019-07-28 17:25:26','2019-07-28 17:26:09'),
(5,1,2,'如何找到靠谱的婚礼摄影师？','导读：掌握这几点秘诀，让你的婚礼照片难忘终生 ',1,1,50,'2019-07-28 17:25:38','2019-07-28 17:26:23');

/*Table structure for table `cms_links` */

DROP TABLE IF EXISTS `cms_links`;

CREATE TABLE `cms_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '名称',
  `url` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '连接',
  `image` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '图片',
  `sort` int(11) NOT NULL DEFAULT '50' COMMENT '排序 小的在前面',
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否有效 1 有效',
  PRIMARY KEY (`id`),
  KEY `state` (`state`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='友情连接';

/*Data for the table `cms_links` */

insert  into `cms_links`(`id`,`title`,`url`,`image`,`sort`,`state`) values 
(1,'优悦婚礼策划','http://www.uwedding.com.cn/',NULL,50,1),
(2,'吕城婚礼策划','http://lvchengwedding.com/',NULL,50,1),
(3,'唐山朵拉婚礼策划','http://www.dorawedding.cn/',NULL,50,1);

/*Table structure for table `cms_page` */

DROP TABLE IF EXISTS `cms_page`;

CREATE TABLE `cms_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '内容',
  `sort_id` int(11) NOT NULL COMMENT '分类',
  `left_id` int(11) NOT NULL DEFAULT '0' COMMENT '左侧内容',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_page` */

insert  into `cms_page`(`id`,`title`,`content`,`sort_id`,`left_id`) values 
(1,'联系我们','官网：www.lovelmd.com<br>\r\n电话：023-66419026/15223403327<br>\r\n客服QQ：1150698811 <br>\r\n地址：重庆市巴南区界石镇腊梅路59号<br>\r\n邮编：400000<br>\r\n	',0,0),
(2,'关于我们','位于重庆市巴南区界石镇海棠村岗上组的重庆罗漫蒂文化传播有限公司于成立，公司主要从事其他行业并主营承办经批准的文化艺术交流活动;婚庆礼仪策划;摄影、摄像服务;设计、制作、代理、发布各类广告。(依法须经批准的项目、经相关部门批准后方可开展经营活动)***，现任法人为余俊材，重庆罗漫蒂文化传播有限公司全体员工以我们相信诚实正直、开拓进取地为公司发展做正确的事情，将为公司和个人带来共同的利益和进步的使命，竭诚为客户提供优质、可靠产品和满意服务。 ',9,1),
(3,'最新优惠','最新优惠',8,1),
(4,'婚礼套餐','婚礼套餐',6,1);

/*Table structure for table `cms_setting` */

DROP TABLE IF EXISTS `cms_setting`;

CREATE TABLE `cms_setting` (
  `keys` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '配置KEY',
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '配置值',
  `title` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '显示名称',
  `stype` tinyint(4) NOT NULL COMMENT '类型 1 数字 2 文本 3 数组 4 对象',
  `remark` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`keys`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `cms_setting` */

insert  into `cms_setting`(`keys`,`value`,`title`,`stype`,`remark`) values 
('globl.page.title','重庆罗漫蒂婚庆','网站标题',2,'网站显示标题'),
('globl.page.keywords','重庆,婚庆,罗漫蒂','关键字',2,'网站关键字'),
('globl.page.description','重庆,婚庆,罗漫蒂','网站描述',2,'网站描述'),
('index.page.icon','/images/icon.png','网站描述',2,'网站描述'),
('index.page.ad','/static/img/ad.png','宣传标语',2,'宣传标语'),
('index.page.phone','023-66419026','联系电话',2,'联系电话'),
('index.page.qq','770507455','联系qq',2,'联系qq'),
('index.page.weibo','https://weibo.com/u/5947731042','联系微博',2,'联系微博'),
('index.page.weixin','重庆,婚庆,罗漫蒂','联系微信',2,'联系微信'),
('index.page.company','重庆罗漫蒂文化传播有限公司','公司名称',2,'公司名称'),
('index.page.url','www.lovelmd.com','网站连接',2,'网站连接'),
('index.page.address','重庆市巴南区界石镇腊梅路59号','联系地址',2,'联系地址'),
('index.page.profile','位于重庆市巴南区界石镇海棠村岗上组的重庆罗漫蒂文化传播有限公司于成立，公司主要从事其他行业并主营承办经批准的文化艺术交流活动;婚庆礼仪策划;摄影、摄像服务;设计、制作、代理、发布各类广告。(依法须经批准的项目、经相关部门批准后方可开展经营活动)***，现任法人为余俊材，重庆罗漫蒂文化传播有限公司全体员工以我们相信诚实正直、开拓进取地为公司发展做正确的事情，将为公司和个人带来共同的利益和进步的使命，竭诚为客户提供优质、可靠产品和满意服务。 	\r\n','公司介绍',2,'公司介绍');

/*Table structure for table `cms_slides` */

DROP TABLE IF EXISTS `cms_slides`;

CREATE TABLE `cms_slides` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '广告标题',
  `description` varchar(128) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '描述内容',
  `image` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '图片连接',
  `url` varchar(200) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '点击之后跳转连接',
  `sort` int(11) NOT NULL DEFAULT '50' COMMENT '排序 小的在前',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态 1 生效',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='首页幻灯片广告';

/*Data for the table `cms_slides` */

insert  into `cms_slides`(`id`,`title`,`description`,`image`,`url`,`sort`,`state`) values 
(1,'11111','新娘更希望拥有一个这样婚礼','/static/img/1.jpg','#',50,1),
(2,'11111','情定心缘怎样为您“私人定制”？','/static/img/2.jpg','#',50,1),
(3,'11111','新娘更希望拥有一个这样婚礼','/static/img/3.jpg','#',50,1),
(4,'11111','情定心缘怎样为您“私人定制”？','/static/img/4.jpg','#',50,1);

/*Table structure for table `cms_sorts` */

DROP TABLE IF EXISTS `cms_sorts`;

CREATE TABLE `cms_sorts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型 0 页面跳转 1 静态单页 2 分类 3 文章 4 文章列表 5 图片列表 6 跳转外链',
  `pid` int(11) NOT NULL DEFAULT '0' COMMENT '上级分类',
  `title` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT '分类标题',
  `data` varchar(128) COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '对应的参数',
  `is_show` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否页面显示',
  `state` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效 1 有效',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='分类（导航）';

/*Data for the table `cms_sorts` */

insert  into `cms_sorts`(`id`,`type`,`pid`,`title`,`data`,`is_show`,`state`) values 
(1,0,0,'首页','/',1,1),
(2,4,0,'婚礼常识','/about',1,1),
(3,2,0,'案例展示',NULL,1,1),
(4,5,3,'高端婚礼策划',NULL,1,1),
(5,5,3,'婚纱摄影基地',NULL,1,1),
(6,1,0,'婚礼套餐',NULL,1,1),
(8,1,0,'最新优惠',NULL,1,1),
(9,1,0,'关于我们',NULL,1,1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
